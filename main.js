import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app.jsx';
import Test from './components/test.jsx';
import Emp from './components/employee.jsx'

ReactDOM.render( <App />, document.getElementById('app'));
ReactDOM.render( <Test data ='51'/>, document.getElementById('test'));
