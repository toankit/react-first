import React from 'react';

class Employee extends React.Component{

    constructor() {
        super();
            this.state={items:[]};
    }
    componentWillMount()
    {

        fetch(`http://jsonplaceholder.typicode.com/posts`)
        .then(result=>result.json())
   .then(items=>this.setState({items}))

    }

    render() {
        
        return(
<div>
            <ul>
            {this.state.items.length ?
                this.state.items.map(item=><li key={item.id}>{item.body}</li>) 
              : <li>Loading...</li>
            }
        </ul>
        
</div>
        );
    }




}

export default Employee;